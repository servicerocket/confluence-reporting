package net.customware.confluence.reporting;


import org.randombits.confluence.filtering.criteria.Criterion;

public abstract class AbstractReportOutput implements ReportOutput {

    private Criterion criterion;

    protected String value;

    protected boolean injected;

    public AbstractReportOutput( String value, boolean injected ) {
        this.value = value;
        this.injected = injected;
    }

    public Criterion getCriterion() {
        return criterion;
    }

    public String getValue() {
        return value;
    }

    public void setCriterion( Criterion criterion ) {
        this.criterion = criterion;
    }

    public void setValue( String value ) {
        this.value = value;
    }

    public boolean isInjected() {
        return injected;
    }

    public void setInjected( boolean injected ) {
        this.injected = injected;
    }

}