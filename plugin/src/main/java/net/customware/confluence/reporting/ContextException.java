package net.customware.confluence.reporting;

/**
 * This is thrown when a report is being run in an unsupported context. Most reports can be run anywhere
 * as long as they are given sufficient detail about where to start.
 */
public class ContextException extends ReportException {

    public ContextException() {
        super();
    }

    public ContextException( String message, Throwable cause ) {
        super( message, cause );
    }

    public ContextException( String message ) {
        super( message );
    }

    public ContextException( Throwable cause ) {
        super( cause );
    }

}
