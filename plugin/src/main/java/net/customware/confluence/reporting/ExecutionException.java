package net.customware.confluence.reporting;

/**
 * This is thrown if there is a problem during the execution of a report chunk,
 * usually from either
 * {@link ReportContext#executeContext(Object, Executor),
 * {@link ReportContext#executeReport(Report, Executor)
 * or
 * {@link ReportContext#executeRoot(com.atlassian.confluence.renderer.PageContext, Executor).
 */
public class ExecutionException extends ReportException {

    public ExecutionException() {
        super();
    }

    public ExecutionException( String message, Throwable cause ) {
        super( message, cause );
    }

    public ExecutionException( String message ) {
        super( message );
    }

    public ExecutionException( Throwable cause ) {
        super( cause );
    }

}
