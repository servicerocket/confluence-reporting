package net.customware.confluence.reporting;

import org.randombits.facade.Facadable;

/**
 * Executors are used with the {@link ReportBuilder} to ensure that reports
 * are built within the correct context and that any exceptions are handled cleanly.
 * 
 * @see ReportBuilder#executeRoot(com.atlassian.confluence.renderer.PageContext, Executor)
 * @see ReportBuilder#executeContext(Object, Executor)
 * @see ReportBuilder#executeReport(Report, Executor)
 */
@Facadable
public interface Executor<T> {
    /**
     * Execute some code.
     * 
     * @return any value.
     * @throws ReportException if there was a problem while executing the code.
     */
    T execute() throws ReportException;
}