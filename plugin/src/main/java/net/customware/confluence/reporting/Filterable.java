package net.customware.confluence.reporting;

import net.customware.confluence.reporting.query.Query;

import org.randombits.confluence.filtering.criteria.Criteria;
import org.randombits.confluence.filtering.criteria.Criterion;
import org.randombits.facade.Facadable;

/**
 * Filter contexts can have {@link Criteria} added to them. {@link Query}
 * implementations are also FilterContexts, which allows them to have filters
 * applied to the values being reported on. Other implementations may also find
 * that ability useful.
 */
@Facadable
public interface Filterable {
    
    /**
     * Adds the specified criterion to the report filters. Some reporters may not support adding
     * criteria, and will return <code>false</code> if this is the case.
     * 
     * @param criterion The new criterion.
     * @return <code>true</code> if the criterion was successfully added.
     */
    boolean addCriterion( Criterion criterion );
    
    /**
     * Returns the combined criteria for this reporter.
     * 
     * @return the criteria.
     */
    Criteria getCriteria();

}
