package net.customware.confluence.reporting;

import org.randombits.confluence.intercom.thing.LocalThingConnection;

public class LocalReportContextConnection extends LocalThingConnection<ReportContext> implements ReportContextConnection {

    public LocalReportContextConnection( ReportContext reportContext ) {
        super( reportContext );
    }

}
