package net.customware.confluence.reporting;

import org.randombits.facade.Facadable;

@Facadable
public interface Outputable<T extends ReportOutput> {
    /**
     * Returns the type the output must match.
     * 
     * @return The output type.
     */
    public Class<T> getOutputType();
    
    /**
     * Attempts to add the output to this context. If the output could not be
     * added, <code>false</code> is returned.
     * 
     * @param output
     *            The output to add.
     * @return <code>true</code> if the output was successfully added.
     */
    public boolean addOutput( @Facadable T output );

    /**
     * Returns <code>true</code> if the the context is set to be injected with
     * keychain values before being rendered.
     * 
     * @return <code>true</code> if the output should be injected.
     */
    public boolean isInjected();
}
