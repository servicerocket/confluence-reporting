package net.customware.confluence.reporting;

import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.Macro;
import org.randombits.facade.Facadable;

@Facadable
public interface ReportContext {
    
    /*
     * (non-Javadoc)
     * 
     * @see net.customware.confluence.reporting.ReportBuilder#isReporting()
     */
    boolean isReporting();

    /*
     * (non-Javadoc)
     * 
     * @see net.customware.confluence.reporting.ReportBuilder#executeRoot(com.atlassian.confluence.renderer.PageContext,
     *      net.customware.confluence.reporting.Executor)
     */
    <T> T executeRoot( PageContext pageContext, Executor<? extends T> executor ) throws ReportException;

    /*
     * (non-Javadoc)
     * 
     * @see net.customware.confluence.reporting.ReportBuilder#executeContext(java.lang.Object,
     *      net.customware.confluence.reporting.Executor)
     */
    <T> T executeContext( Object context, Executor<? extends T> executor ) throws ReportException;

    /*
     * (non-Javadoc)
     * 
     * @see net.customware.confluence.reporting.ReportBuilder#executeReport(net.customware.confluence.reporting.Report,
     *      net.customware.confluence.reporting.Executor)
     */
    <T> T executeReport( Report<?, ?> report, Executor<? extends T> executor ) throws ReportException;

    /*
     * (non-Javadoc)
     * 
     * @see net.customware.confluence.reporting.ReportBuilder#getCurrentReport()
     */
    Report<?, ?> getCurrentReport() throws ReportException;

    Object getCurrentReportItem() throws ReportException;

    /*
     * (non-Javadoc)
     * 
     * @see net.customware.confluence.reporting.ReportBuilder#getCurrentContext()
     */
    Object getCurrentContext() throws ReportException;

    /*
     * (non-Javadoc)
     * 
     * @see net.customware.confluence.reporting.ReportBuilder#getCurrentContext(java.lang.Class)
     */
    <C> C getCurrentContext( Class<C> targetClass ) throws ReportException;

    /*
     * (non-Javadoc)
     * 
     * @see net.customware.confluence.reporting.ReportBuilder#getRootPageContext()
     */
    PageContext getRootPageContext() throws ReportException;

    /*
     * (non-Javadoc)
     * 
     * @see net.customware.confluence.reporting.ReportBuilder#getCurrentPageContext()
     */
    PageContext getCurrentPageContext() throws ReportException;

    /*
     * (non-Javadoc)
     * 
     * @see net.customware.confluence.reporting.ReportBuilder#renderWiki(java.lang.String,
     *      com.atlassian.renderer.v2.RenderMode,
     *      net.customware.confluence.reporting.macro.AbstractReportingMacro)
     */
    String renderWiki( String wiki, RenderMode renderMode, Macro macro ) throws ReportException;

}