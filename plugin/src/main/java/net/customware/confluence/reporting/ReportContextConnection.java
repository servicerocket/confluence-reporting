package net.customware.confluence.reporting;

import org.randombits.confluence.intercom.thing.ThingConnection;

public interface ReportContextConnection extends ThingConnection<ReportContext> {

}
