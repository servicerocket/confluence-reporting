package net.customware.confluence.reporting;

import org.randombits.confluence.intercom.thing.ThingFinder;

public class ReportContextFinder extends ThingFinder<ReportContext, ReportContextConnection> {

    public ReportContextFinder() {
        super( ReportContext.class, ReportContextConnection.class );
    }

}
