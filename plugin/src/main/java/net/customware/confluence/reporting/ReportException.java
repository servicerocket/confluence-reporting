package net.customware.confluence.reporting;

/**
 * This is the root exception type for problems during reporting.
 */
public class ReportException extends Exception {

    public ReportException() {
    }

    public ReportException( String message ) {
        super( message );
    }

    public ReportException( Throwable cause ) {
        super( cause );
    }

    public ReportException( String message, Throwable cause ) {
        super( message, cause );
    }

}
