package net.customware.confluence.reporting;

import org.randombits.confluence.filtering.criteria.Criterion;
import org.randombits.facade.Facadable;

/**
 * This class defines what a chunk of output on the current report item will
 * look like. The actual Report will output the actual HTML, but this class
 * contains any parameters, wiki markup and criterion the output should be based
 * on.
 */
@Facadable
public interface ReportOutput {

    /**
     * The value to use to generate the HTML for the current item. Often wiki
     * markup.
     */
    String getValue();

    /**
     * Sets the value of the output chunk.
     * 
     * @param value
     */
    void setValue( String value );

    /**
     * Sets the criterion the current item must match for this output chunk to
     * be displayed. If no criterion is set the output will be displayed.
     * 
     * @param criterion
     *            the criterion.
     */
    void setCriterion( @Facadable Criterion criterion );

    /**
     * Returns the criterion the current item must match for this output chunk
     * to be displayed. If no criterion is set the output will be displayed.
     * 
     * @return the criterion.
     */
    @Facadable Criterion getCriterion();

    /**
     * If an output is injected, the %prefix:key% items should be injected into
     * the value prior to rendering.
     * 
     * @return <code>true</code> if supplier values are injected.
     */
    boolean isInjected();

    /**
     * Sets if supplier values should be injected into the value of this output.
     * 
     * @param injected
     */
    void setInjected( boolean injected );
}