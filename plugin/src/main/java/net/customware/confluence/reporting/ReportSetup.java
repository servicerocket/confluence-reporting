package net.customware.confluence.reporting;

import java.util.List;

import org.randombits.facade.Facadable;

import net.customware.confluence.reporting.query.Query;
import net.customware.confluence.reporting.query.Queryable;

@Facadable
public interface ReportSetup<O extends ReportOutput> extends Queryable<Object>, Outputable<O>, Filterable {
    
    /**
     * Returns the CSS class name for the overall report. This can be used to
     * provide custom style information for the report.
     * 
     * @return The CSS class name.
     */
    String getCssClass();

    /**
     * Sets the CSS class name for the overall report. This can be used to
     * provide custom style information for the report.
     * 
     * @param cssClass
     *            The new CSS class name.
     */
    void setCssClass( String cssClass );

    /**
     * Returns the wiki markup to display if the report is empty (i.e. has no
     * items returned from the query.
     * 
     * @return the empty wiki markup.
     */
    String getEmpty();

    /**
     * Sets the wiki markup to display if the report is empty.
     * 
     * @param empty
     *            the empty wiki markup.
     */
    void setEmpty( String empty );

    /**
     * Returns the wiki markup to display at the end of a report which has at
     * least 1 result to display.
     * 
     * @return the footer wiki markup.
     */
    String getFooter();

    /**
     * Sets the footer wiki markup.
     * 
     * @param footer
     *            the footer wiki markup.
     * @see #getFooter()
     */
    void setFooter( String footer );

    /**
     * Returns the wiki markup to display at the beginning of a report which has
     * at least 1 result to display.
     * 
     * @return the header wiki markup.
     */
    String getHeader();

    /**
     * Sets the header wiki markup.
     * 
     * @param header
     *            the header wiki markup.
     * @see #getHeader()
     */
    void setHeader( String header );

    boolean isMatchAll();

    /**
     * Values can have 'children', which are supplied via the
     * SupplierAssistant's <code>'findChildren'</code> method. These children
     * can be recursed through and output at a different level, or however a
     * report may wish. A depth of 0 means displaying no children, 1 is the
     * first generation, and so on.
     * 
     * @return the depth to go to when displaying descendents.
     */
    int getDepth();

    /**
     * Returns the maximum number of results to display at the top level. Note
     * that this does not include any decendents which may be being output if
     * 'depth' is greater than 0. All valid decendents are displayed regardless
     * of this value if that is the case.
     */
    int getMaxResults();

    /**
     * The number of the first result to display. The first result has a number
     * of <code>1</code>.
     */
    int getFirstResult();

    /**
     * If this is set to <code>true</code>, the body will be injected with
     * values from the current context, using the '%prefix:keychain%' markup
     * standard by default. This can be overridden by individual
     * {@link ReportOutput} macros
     */
    boolean isInjected();

    /**
     * Indicates whether decendents should be sorted with the same rules as the
     * top-level report. Defaults to <code>true</code>.
     */
    boolean isSortDescendents();

    /**
     * Returns the list of outputs added to this report.
     */
    public abstract @Facadable List<O> getOutputs();

    /**
     * Returns the {@link Query} for this setup.
     * 
     * @return The query.
     */
    public abstract Query<? extends Object> getQuery();
}