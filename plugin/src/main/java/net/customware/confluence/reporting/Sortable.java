package net.customware.confluence.reporting;

import java.util.Comparator;

import org.randombits.facade.Facadable;

/**
 * Sortable values can have comparators added to control the display order.
 */
@Facadable
public interface Sortable<V> {

    /**
     * Adds the specified comparator to this sortable instance. If other
     * comparators had already been added, it is added after the others.
     * 
     * @param comparator
     *            The new comparator.
     * @return <code>true</code> if the comparator was successfully added.
     */
    boolean addComparator( Comparator<? super V> comparator );

    /**
     * Returns the comparator for the sortable instance. It is a cascading chain
     * of all previously-added comparators, or <code>null</code> if none have
     * been specified.
     * 
     * @return the comparator chain.
     */
    Comparator<? super V> getComparator();

}
