/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.reporting.macro;

import net.customware.confluence.reporting.Filterable;
import net.customware.confluence.reporting.ReportBuilder;
import net.customware.confluence.reporting.ReportException;
import net.customware.confluence.reporting.supplier.SupplierValueCriterion;

import org.randombits.confluence.filtering.criteria.Criterion;
import org.randombits.confluence.support.MacroInfo;
import org.randombits.storage.IndexedStorage;

import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;

/**
 * Filter macros set up {@link Criterion} to filter report items out.
 * 
 * @author David Peterson
 */
public abstract class AbstractFilterMacro extends AbstractReportingMacro {
    private static final String KEY_PARAM = "key";

    /**
     * Creates the {@link Criterion} for this filter type based on the macro
     * parameters and body.
     * 
     * @param info
     *            the macro info.
     * @return the new {@link Criterion}.
     * @throws MacroException
     *             if there is a problem with the macro parameters.
     * @throws ReportException
     *             if there is a problem processing the report.
     */
    protected abstract Criterion createCriterion( MacroInfo info ) throws MacroException, ReportException;

    /**
     * This executes the macro in the current context. Generally you will not
     * have to override this method.
     */
    @Override protected String report( MacroInfo info ) throws MacroException, ReportException {
        Filterable filterable;
        filterable = ReportBuilder.getFilterable();
        if ( filterable == null )
            throw new MacroException( "This macro cannot be used in this context." );

        IndexedStorage params = info.getMacroParams();

        String key = params.getString( KEY_PARAM, params.getString( 0, null ) );

        Criterion criterion = createCriterion( info );

        filterable.addCriterion( new SupplierValueCriterion( key, criterion ) );

        return "";
    }

    /**
     * Returns <code>false</code>.
     */
    public final boolean isInline() {
        return false;
    }

    /**
     * Returns <code>false</code>. Override this method if you want to process the filter body.
     */
    public boolean hasBody() {
        return false;
    }

    /**
     * @return {@link RenderMode#NO_RENDER}.
     */
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }
}
