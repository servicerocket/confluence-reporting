/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.reporting.macro;

import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import net.customware.confluence.reporting.*;
import net.customware.confluence.reporting.*;
import net.customware.confluence.reporting.query.Query;
import net.customware.confluence.reporting.query.QueryException;
import net.customware.confluence.reporting.query.Results;
import org.randombits.confluence.filtering.criteria.Criterion;
import org.randombits.confluence.supplier.SupplierAssistant;
import org.randombits.confluence.supplier.SupplierException;
import org.randombits.confluence.supplier.UnsupportedContextException;
import org.randombits.confluence.support.MacroInfo;

import java.util.*;

/**
 * The default base class for report macros. Simply implementing the abstract
 * methods will work for most report types. The order that the 'handle' methods
 * are called is as follows:
 * 
 * <table>
 * <tr>
 * <th><b>Method</b></th>
 * <th><b>Description</b></th>
 * </tr>
 * 
 * <tr>
 * <td>{@link #handleBeforeReport(StringBuffer, net.customware.confluence.reporting.Report, MacroInfo)}</td>
 * <td>This is called once <i>after</i> outputting the header, but before any
 * items are processed.</td>
 * </tr>
 * 
 * <tr>
 * <td>{@link #handleBeforeItems(StringBuffer, net.customware.confluence.reporting.Report, MacroInfo, int)}</td>
 * <td>This is called before processing a list of items, which could be a list
 * of decendents if the report supports it.</td>
 * </tr>
 * 
 * <tr>
 * <td>{@link #handleBeforeItem(Object, StringBuffer, net.customware.confluence.reporting.Report, MacroInfo, int)}</td>
 * <td>This is called before processing a single item or row of a report.</td>
 * </td>
 * 
 * <tr>
 * <td>{@link #handleOutput(net.customware.confluence.reporting.ReportOutput, StringBuffer, net.customware.confluence.reporting.Report, org.randombits.confluence.support.MacroInfo)}</td>
 * <td>This is called once for each {@link net.customware.confluence.reporting.ReportOutput} registered on a
 * report. Some reports will just output the rendered report output value, while
 * some will want to output extra HTML markup.</td>
 * </tr>
 * 
 * <tr>
 * <td>{@link #handleAfterItem(Object, StringBuffer, net.customware.confluence.reporting.Report, MacroInfo, int)}</td>
 * <td>This is called after outputting each item.</td>
 * </tr>
 * 
 * <tr>
 * <td>{@link #handleBetweenItems(StringBuffer, net.customware.confluence.reporting.Report, MacroInfo, int)}</td>
 * <td>This method is only called if an item was just processed an another one
 * is about to be. It will not be called after the last item in a list of items.
 * It is also called before <code>handleBeforeItem</code> is called for the
 * next item.</td>
 * </tr>
 * 
 * <tr>
 * <td>{@link #handleAfterItems(StringBuffer, net.customware.confluence.reporting.Report, MacroInfo, int)}</td>
 * <td>This is called at the end of a list of items.</td>
 * </tr>
 * 
 * <tr>
 * <td>{@link #handleAfterReport(StringBuffer, net.customware.confluence.reporting.Report, MacroInfo)}</td>
 * <td>This is called after the report has processed, but before the report
 * footer value.</td>
 * </tr>
 * </table>
 * 
 * @author David Peterson
 */
public abstract class AbstractReportMacro<Setup extends ReportSetup<Output>, Output extends ReportOutput> extends
        AbstractReportingMacro {

    private static final RenderMode RENDER_MODE = RenderMode.suppress( RenderMode.F_FIRST_PARA );

    /**
     * Executes the report. Generally you will not need to override this method.
     */
    @Override
    protected String report( final MacroInfo info ) throws MacroException, ReportException {
        // Attempts to set the current content as the root.
        return ReportBuilder.executeRoot( info.getPageContext(), new Executor<String>() {

            public String execute() throws ReportException {

                final Setup setup = createReportSetup( info );
                String error = initSetup( setup, info );
                if ( error != null ) {
                    return error;
                }

                // Check we can actually output...
                List<Output> outputs = setup.getOutputs();
                if ( outputs == null || outputs.size() == 0 )
                    throw new ExecutionException( "Please supply the report output setup." );

                // Check we have a query to execute.
                Query<?> query = setup.getQuery();
                if ( query == null )
                    throw new ExecutionException( "Please supply a reporter macro." );

                // Start a new report.
                final Report<Setup, Output> report = createReport( setup, info );
                report.setParent( ReportBuilder.getCurrentReport() );

                final Results<?> results;
                try {
                    results = query.execute();

                    String value = ReportBuilder.executeReport( report, new Executor<String>() {

                        public String execute() throws ReportException {

                            try {
                                StringBuffer out = new StringBuffer();
                                int count = 1;
                                int firstResult = report.getSetup().getFirstResult();

                                // Jump through any initial results until we get
                                // to firstResult.
                                while ( results.hasNext() && ( count < firstResult ) ) {
                                    results.next();
                                    count++;
                                }

                                if ( results.hasNext() ) {
                                    // First, output the header
                                    String header = setup.getHeader();
                                    if ( header != null )
                                        out.append( render( header ) );

                                    // Next, output the 'before' markup.
                                    handleBeforeReport( out, report, info );

                                    processItems( results, out, report, info, report.getSetup().getDepth() );

                                    handleAfterReport( out, report, info );

                                    String footer = setup.getFooter();
                                    if ( footer != null )
                                        out.append( render( footer ) );
                                } else {
                                    String empty = setup.getEmpty();
                                    if ( empty != null )
                                        out.append( render( empty ) );
                                }

                                return out.toString();
                            } catch ( SupplierException e ) {
                                throw new ExecutionException( "Error processing the report: " + e.getMessage(), e );
                            } catch ( UnsupportedContextException e ) {
                                throw new ExecutionException( "Error processing the report: " + e.getMessage(), e );
                            }
                        }
                    } );

                    results.close();

                    return value;

                } catch ( QueryException e ) {
                    throw new ExecutionException( e.getMessage(), e );
                }

            }
        } );
    }

    private String initSetup( ReportSetup<Output> setup, final MacroInfo info ) throws ReportException {
        return ReportBuilder.executeContext( setup, new Executor<String>() {
            public String execute() throws ReportException {

                // Render to body to collect information about data, settings,
                // etc.
                String rendered = renderWiki( info.getMacroBody(), RENDER_MODE );
                if ( rendered != null && rendered.trim().length() > 0 ) {
                    // There was a problem.
                    return rendered;
                }
                return null;

            }
        } );
    }

    /**
     * Creates the {@link ReportSetup} instance for this report type. Each
     * implementation generally has its own specific subclass of ReportSetup
     * with any report-type-specific options.
     * 
     * @param info
     *            The macro info.
     * @return the new report setup.
     * @throws ReportException
     *             if there were any issues processing the parameters.
     */
    protected abstract Setup createReportSetup( MacroInfo info ) throws ReportException;

    private void processItems( Iterator<?> i, StringBuffer out, Report<Setup, Output> report, MacroInfo info,
            int depth ) throws SupplierException, ReportException, UnsupportedContextException {
        int maxResults = report.getSetup().getMaxResults();
        int count = 0;

        // Do any depth-increasing stuff
        handleBeforeItems( out, report, info, depth );

        while ( i.hasNext() && ( maxResults < 1 || count < maxResults ) ) {
            count++;

            Object item = i.next();

            if ( count > 1 )
                handleBetweenItems( out, report, info, depth );

            processItem( item, out, report, info, depth );
        }

        handleAfterItems( out, report, info, depth );
    }

    /**
     * This method is executed between the processing of each item.
     * 
     * @param out The output buffer.
     * @param report The report.
     * @param info The macro info.
     * @param depth The depth currently being processed.
     * @throws ReportException if there is a problem.
     */
    protected abstract void handleBetweenItems( StringBuffer out, Report<Setup, Output> report, MacroInfo info,
            int depth ) throws ReportException;

    /**
     * This method is called just after looping through the items in the current
     * depth. This includes the initial loop, as well as any descendent loops.
     * 
     * @param out
     *            The output buffer.
     * @param report
     *            The report.
     * @param info
     *            The macro info.
     * @param currentDepth
     *            The current depth.
     * @throws ReportException
     *             if there is a problem processing the report.
     */
    protected abstract void handleAfterItems( StringBuffer out, Report<Setup, Output> report, MacroInfo info,
            int currentDepth ) throws ReportException;

    /**
     * This method is called just prior to looping through the items in the
     * current depth. This includes the initial loop, as well as any descendent
     * loops.
     * 
     * @param out
     *            The output buffer.
     * @param report
     *            The report.
     * @param info
     *            The macro info.
     * @param currentDepth
     *            The current depth.
     * @throws ReportException
     *             if there is a problem processing the report.
     */
    protected abstract void handleBeforeItems( StringBuffer out, Report<Setup, Output> report, MacroInfo info,
            int currentDepth ) throws ReportException;

    private void processItem( Object item, StringBuffer out, Report<Setup, Output> report, MacroInfo info,
            int currentDepth ) throws SupplierException, ReportException, UnsupportedContextException {
        report.setCurrentItem( item );

        handleBeforeItem( item, out, report, info, currentDepth );

        // Then, go through the outputs one at a time.
        for ( Output output : report.getOutputs() ) {
            handleBeforeOutput( output, out, report, info );
            Criterion criterion = output.getCriterion();
            if ( criterion == null || criterion.matches( item ) ) {
                handleOutput( output, out, report, info );
            }
            handleAfterOutput( output, out, report, info );
        }

        handleAfterItem( item, out, report, info, currentDepth );

        if ( currentDepth > 0 ) {
            Collection<?> children;
            try {
                children = SupplierAssistant.getInstance().findChildren( item, null );
            } catch ( SupplierException e ) {
                throw new ExecutionException( "A problem occurred while retrieving children: " + e.getMessage(), e );
            } catch ( UnsupportedContextException e ) {
                throw new ExecutionException( "A problem occurred while retrieving children: " + e.getMessage(), e );
            }

            if ( children != null ) {
                Comparator<? super Object> comparator = null;
                if ( report.getSetup().isSortDescendents() && report.getQuery() instanceof Sortable)
                    comparator = ( ( Sortable<Object> ) report.getQuery() ).getComparator();

                if ( comparator != null ) {
                    List<Object> list = new java.util.ArrayList<Object>( children );
                    Collections.sort( list, comparator );
                    children = list;
                }

                processItems( children.iterator(), out, report, info, currentDepth - 1 );
            }
        }
    }

    /**
     * This method is called after processing each individual output via the
     * <code>handleOutput</code> method.
     * 
     * @param output
     *            The output which was processed.
     * @param out
     *            The output buffer.
     * @param report
     *            The report.
     * @param info
     *            The macro info.
     * @throws ReportException
     *             if there is a problem.
     * 
     * @see #handleOutput(net.customware.confluence.reporting.ReportOutput, StringBuffer, net.customware.confluence.reporting.Report, org.randombits.confluence.support.MacroInfo)
     * @see #handleAfterOutput(net.customware.confluence.reporting.ReportOutput, StringBuffer, net.customware.confluence.reporting.Report, org.randombits.confluence.support.MacroInfo)
     */
    protected abstract void handleAfterOutput( Output output, StringBuffer out, Report<Setup, Output> report,
            MacroInfo info ) throws ReportException;

    /**
     * This method is called prior to processing each individual output via the
     * <code>handleOutput</code> method.
     * 
     * @param output
     *            The output which will be processed.
     * @param out
     *            The output buffer.
     * @param report
     *            The report.
     * @param info
     *            The macro info.
     * 
     * @throws ReportException
     *             if there is a problem.
     * 
     * @see #handleOutput(net.customware.confluence.reporting.ReportOutput, StringBuffer, net.customware.confluence.reporting.Report, org.randombits.confluence.support.MacroInfo)
     * @see #handleAfterItem(Object, StringBuffer, Report, MacroInfo, int)
     */
    protected abstract void handleBeforeOutput( Output output, StringBuffer out, Report<Setup, Output> report,
            MacroInfo info ) throws ReportException;

    /**
     * This method is called prior to processing the {@link ReportOutput} list
     * registered for the report.
     * 
     * @param item
     *            The item being reported on.
     * @param out
     *            The output buffer.
     * @param report
     *            The report.
     * @param info
     *            The macro info.
     * @param currentDepth
     *            The current depth.
     * @throws ReportException
     *             if there is a problem processing the report.
     */
    protected abstract void handleBeforeItem( Object item, StringBuffer out, Report<Setup, Output> report,
            MacroInfo info, int currentDepth ) throws ReportException;

    /**
     * This method is called after processing the {@link ReportOutput}s
     * registered for the report.
     * 
     * @param item
     *            The item being reported on.
     * @param out
     *            The output buffer.
     * @param report
     *            The report.
     * @param info
     *            The macro info.
     * @param currentDepth
     *            The current depth.
     * @throws ReportException
     *             if there is a problem processing the report.
     */
    protected abstract void handleAfterItem( Object item, StringBuffer out, Report<Setup, Output> report,
            MacroInfo info, int currentDepth ) throws ReportException;

    /**
     * This method is called once for each ReportOutput value in the report, on
     * each report item. It is only called if the output's 'criterion' matches
     * the report item.
     * 
     * @param output
     *            output definition.
     * @param out
     *            The StringBuffer to output any XHTML into.
     * @param report
     *            The report.
     * @param info
     *            The macro info.
     * @throws SupplierException
     *             if there is a problem processing suppliers.
     * @throws ReportException if there is an error while reporting.
     * @throws UnsupportedContextException if the current context is unsupported by the report value.
     */
    protected void handleOutput( Output output, StringBuffer out, Report<Setup, Output> report, MacroInfo info )
            throws SupplierException, ReportException, UnsupportedContextException {
        out.append( renderOutput( output, report, getOutputRenderMode() ) );
    }

    /**
     * Returns the {@link RenderMode} to use for the output chunks.
     * 
     * @return The render mode.
     */
    protected abstract RenderMode getOutputRenderMode();

    /**
     * This method is called after looping through the report items, but prior
     * to the 'footer' being output. It should be used to close off any XHTML
     * required for the report type.
     * 
     * @param out
     *            The StringBuffer to put any XHTML into.
     * @param report
     *            The report.
     * @param info
     *            The macro info.
     * @throws ReportException
     *             if there is a problem.
     */
    protected abstract void handleAfterReport( StringBuffer out, Report<Setup, Output> report, MacroInfo info )
            throws ReportException;

    /**
     * This method is called after outputting the 'header' value, but prior to
     * looping through the items in the report. It should be used for outputting
     * any preceding HTML required for the report type.
     * 
     * @param out
     *            The StringBuffer to put any XHTML into.
     * @param report
     *            The report.
     * @param info
     *            The macro info.
     * @throws ReportException if there is a problem.
     */
    protected abstract void handleBeforeReport( StringBuffer out, Report<Setup, Output> report, MacroInfo info )
            throws ReportException;

    /**
     * Called to create the report instance for this macro.
     * 
     * @param setup
     *            The report setup.
     * @param info
     *            The macro info.
     * @return the new report.
     * @throws ExecutionException if there is a problem.
     *             if there is a problem.
     */
    protected Report<Setup, Output> createReport( Setup setup, MacroInfo info ) throws ExecutionException {
        return new DefaultReport<Setup, Output>( setup );
    }

    private String render( String wikiText ) throws ReportException {
        return renderWiki( wikiText, RENDER_MODE );
    }

    /**
     * Returns <code>false</code>.
     */
    public boolean isInline() {
        return false;
    }

    /**
     * Returns <code>true</code>.
     */
    public boolean hasBody() {
        return true;
    }

    /**
     * Returns {@link RenderMode#NO_RENDER}.
     */
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    /**
     * This method will render the value of the specified output in the context
     * of the current report item, taking into account whether the value should
     * be injected or not prior to rendering.
     * 
     * @param output
     *            The current output.
     * @param report
     *            The current report.
     * @param renderMode
     *            The render mode to use when rendering macros.
     * @return The rendered, potentially injected values.
     * @throws SupplierException
     *             if there was a problem injecting supplier values.
     * @throws ReportException
     *             if there is a problem while handling the report.
     * @throws UnsupportedContextException if the current context is unsupported.
     */
    protected String renderOutput( Output output, Report<Setup, Output> report, RenderMode renderMode )
            throws SupplierException, ReportException, UnsupportedContextException {
        Object value = getInjectedValue( output, report.getCurrentItem() );
        return renderWiki( value == null ? "" : value.toString(), renderMode );
    }

    /**
     * Returns the unrendered value text for the output with any %prefix:key%
     * values injected with the value in the specified context.
     * 
     * @param output
     *            The current output object.
     * @param context
     *            The current context.
     * @return the injected value string.
     * @throws SupplierException
     *             if there was a problem injecting supplier values.
     * @throws UnsupportedContextException if the context is unsupported in the current context.
     */
    protected Object getInjectedValue( Output output, Object context ) throws SupplierException,
            UnsupportedContextException {
        Object value = output.getValue();
        if ( output.isInjected() )
            value = SupplierAssistant.getInstance().injectValues( context, output.getValue() );
        return value;
    }
}
