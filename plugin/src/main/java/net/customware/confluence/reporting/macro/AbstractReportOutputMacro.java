package net.customware.confluence.reporting.macro;

import net.customware.confluence.reporting.Executor;
import net.customware.confluence.reporting.Outputable;
import net.customware.confluence.reporting.ReportBuilder;
import net.customware.confluence.reporting.ReportException;
import net.customware.confluence.reporting.ReportOutput;

import org.randombits.confluence.support.MacroInfo;

import com.atlassian.renderer.v2.macro.MacroException;

/**
 * This is the base class for macros which define a {@link ReportOutput} for a
 * report. Different report types will often have different implementations of
 * {@link ReportOutput} which have specific options for that report type.
 */
public abstract class AbstractReportOutputMacro<O extends ReportOutput> extends AbstractReportingMacro {

    private Class<O> outputType;
    
    public AbstractReportOutputMacro( Class<O> outputType ) {
        this.outputType = outputType;
    }
    
    /**
     * Implements the standard report method.
     */
    @Override protected final String report( final MacroInfo info ) throws MacroException, ReportException {
        final Outputable<O> outputable = ReportBuilder.getOutputable( outputType );

        if ( outputable == null )
            throw new MacroException( "This macro cannot be used in this context." );

        return ReportBuilder.executeContext( null, new Executor<String>() {

            public String execute() throws ReportException {
                return AbstractReportOutputMacro.this.report( outputable, info );
            }
        } );
    }

    /**
     * This method is called to allow setting of a new {@link ReportOutput} into
     * the current {@link Outputtable}.
     * 
     * @param outputable
     *            The target outputtable.
     * @param info
     *            The macro info.
     * @return Successful macros should return an empty string (""). Otherwise
     *         it will be considered to be an error and will be output to the
     *         browser, causing the rest of the report to fail.
     * @throws ReportException if there is a problem with the report.
     */
    protected abstract String report( Outputable<O> outputable, MacroInfo info ) throws ReportException;
}
