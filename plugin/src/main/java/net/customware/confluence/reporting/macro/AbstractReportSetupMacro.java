package net.customware.confluence.reporting.macro;

import net.customware.confluence.reporting.Executor;
import net.customware.confluence.reporting.ReportBuilder;
import net.customware.confluence.reporting.ReportException;
import net.customware.confluence.reporting.ReportOutput;
import net.customware.confluence.reporting.ReportSetup;

import org.randombits.confluence.support.MacroInfo;

import com.atlassian.renderer.v2.macro.MacroException;

/**
 * This is the base class for macros which need access to the
 * {@link ReportSetup} during the construction phase of the report.
 */
public abstract class AbstractReportSetupMacro<O extends ReportOutput> extends AbstractReportingMacro {
    
    private Class<O> outputType;
    
    public AbstractReportSetupMacro( Class<O> outputType ) {
        this.outputType = outputType;
    }

    @Override protected final String report( final MacroInfo info ) throws MacroException, ReportException {
        final ReportSetup<O> setup = ReportBuilder.getReportSetup( outputType );

        if ( setup == null )
            throw new MacroException( "This macro must be contained in a report." );

        return ReportBuilder.executeContext( null, new Executor<String>() {
            public String execute() throws ReportException {
                return AbstractReportSetupMacro.this.report( setup, info );
            }
        } );
    }

    /**
     * Implement this method to get access to the {@link ReportSetup}.
     * 
     * @param setup
     *            The report setup.
     * @param info
     *            The macro info.
     * @return the value to output to the browser. This should be a blank string
     *         (""), or it will be treated as an error message and the report
     *         will fail.
     * @throws ReportException
     *             if there is a problem with the report.
     */
    protected abstract String report( ReportSetup<O> setup, MacroInfo info ) throws ReportException;
}
