/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.reporting.macro;

import net.customware.confluence.reporting.Executor;
import net.customware.confluence.reporting.ReportBuilder;
import net.customware.confluence.reporting.ReportException;
import net.customware.confluence.reporting.query.Query;
import net.customware.confluence.reporting.query.QueryException;
import net.customware.confluence.reporting.query.Queryable;

import org.randombits.confluence.support.MacroInfo;

import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.SubRenderer;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.spring.container.ContainerManager;

/**
 * The abstract base-class for reporter macros. Reporters are responsible for
 * executing a query of some sort and returning the list of values which were
 * found, which can then be displayed by the {@link AbstractReportMacro}
 * implementations.
 * 
 * @author David Peterson
 * 
 * @param <T> The valueType of object produced by the reporter.
 */
public abstract class AbstractReporterMacro<T> extends AbstractReportingMacro {
    protected static final String MATCH_ALL_PARAM = "matchAll";

    private SubRenderer subRenderer;
    
    private Class<T> valueType;
    
    public AbstractReporterMacro( Class<T> valueType ) {
        this.valueType = valueType;
    }

    /**
     * Executes the reporter macro. Generally you will not have to override this
     * method.
     */
    @Override protected String report( final MacroInfo info ) throws MacroException, ReportException {

        Queryable<T> queryable = ReportBuilder.getQueryable( valueType );
        if ( queryable == null )
            throw new MacroException( "This reporter cannot be used in this context." );

        if ( !queryable.canAddQuery() )
            throw new MacroException( "No further reporters can be specified in this context." );

        Query<T> query;
        try {
            query = createQuery( info );
        } catch ( QueryException e ) {
            throw new MacroException( e );
        }

        ContainerManager.autowireComponent( query );

        String result;

        result = ReportBuilder.executeContext( query, new Executor<String>() {
            public String execute() {
                String result = subRenderer.render( info.getMacroBody(), info.getPageContext(), RenderMode.ALL );
                if ( result != null && result.trim().length() > 0 )
                    return result;
                return null;
            }
        } );

        if ( result == null )
            queryable.addQuery( query );

        return result == null ? "" : result;
    }

    /**
     * Creates the query for this macro. The query will be automatically
     * autowired after creation.
     * 
     * @param info
     *            The macro parameters.
     * @return The new Reporter instance.
     * @throws QueryException
     */
    protected abstract Query<T> createQuery( MacroInfo info ) throws QueryException, ReportException;

    /**
     * Returns false. May not be overridden.
     */
    public final boolean isInline() {
        return false;
    }

    /**
     * Returns <code>true</code>.
     */
    public boolean hasBody() {
        return true;
    }

    /**
     * @return {@link RenderMode#NO_RENDER}
     */
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    /**
     * Sets the {@link SubRenderer} for this macro.
     * 
     * @param subRenderer
     *            the {@link SubRenderer}
     */
    public void setSubRenderer( SubRenderer subRenderer ) {
        this.subRenderer = subRenderer;
    }
}
