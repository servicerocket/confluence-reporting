/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.reporting.macro;

import java.util.Comparator;

import net.customware.confluence.reporting.ReportBuilder;
import net.customware.confluence.reporting.ReportException;
import net.customware.confluence.reporting.Sortable;
import net.customware.confluence.reporting.supplier.SupplierValueComparator;

import org.apache.commons.collections.comparators.ReverseComparator;
import org.randombits.confluence.support.MacroInfo;
import org.randombits.storage.IndexedStorage;

import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;

/**
 * Abstract base class for sort macros. Subclasses should return values which
 * sort in default order based on specific data types. The macro will wrap any
 * 
 * @author David Peterson
 */
public abstract class AbstractSortMacro<V> extends AbstractReportingMacro {
    private static final String KEY_PARAM = "key";

    private static final String ORDER_PARAM = "order";

    private static final String ASC_ORDER = "asc";

    private static final String DESC_ORDER = "desc";

    private static final String RENDER_PARAM = "render";

    private static final String WIKI_RENDER = "wiki";

    /**
     * Executes the sort macro.
     * 
     * @return the XHTML to display as an error message.
     * @throws MacroException
     *             if there is a problem with the macro configuration.
     * @throws ReportException
     *             if there is a problem handling the report.
     */
    @Override protected String report( MacroInfo info ) throws MacroException, ReportException {

        Sortable<?> sortable = ReportBuilder.getSortable();

        if ( sortable == null )
            throw new MacroException( "This macro must be used in a sortable location, such as a reporter macro." );

        IndexedStorage params = info.getMacroParams();

        String key = params.getString( KEY_PARAM, params.getString( 0, null ) );
        boolean renderWiki = WIKI_RENDER.equals( params.getString( RENDER_PARAM, null ) );

        Comparator<? super V> comparator = createComparator( info );

        if ( comparator != null ) {
            Comparator<Object> supplierComparator = new SupplierValueComparator<Object, V>( key, comparator, renderWiki );

            if ( params.getString( ORDER_PARAM, ASC_ORDER ).toLowerCase().startsWith( DESC_ORDER ) )
                supplierComparator = new ReverseComparator( supplierComparator );

            sortable.addComparator( supplierComparator );

            return "";
        } else {
            throw new MacroException( "No comparator was created." );
        }
    }

    /**
     * Creates the comparator for this macro.
     * 
     * @param info
     *            the macro info.
     * @return the new Comparator.
     * @throws MacroException
     *             if there are any problems with the macro configuration.
     * @throws ReportException
     *             if there are any problems related to the report.
     */
    protected abstract Comparator<V> createComparator( MacroInfo info ) throws MacroException, ReportException;

    public boolean isInline() {
        return false;
    }

    public boolean hasBody() {
        return false;
    }

    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }
}
