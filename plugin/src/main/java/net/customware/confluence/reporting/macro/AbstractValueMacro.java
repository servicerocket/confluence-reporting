/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.reporting.macro;

import net.customware.confluence.reporting.ExecutionException;
import net.customware.confluence.reporting.Executor;
import net.customware.confluence.reporting.ReportBuilder;
import net.customware.confluence.reporting.ReportException;

import org.randombits.confluence.filtering.param.ParameterException;
import org.randombits.confluence.filtering.param.content.ConfluenceEntityParameter;
import org.randombits.confluence.supplier.SupplierAssistant;
import org.randombits.confluence.supplier.SupplierException;
import org.randombits.confluence.supplier.UnsupportedContextException;
import org.randombits.confluence.support.MacroInfo;
import org.randombits.storage.IndexedStorage;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.renderer.v2.macro.MacroException;

/**
 * The base class for macros which will display a specific value.
 * 
 * @author David Peterson
 */
public abstract class AbstractValueMacro extends AbstractReportingMacro {
    protected static final String KEY_PARAM = "key";

    @Override
    protected String report( final MacroInfo info ) throws MacroException, ReportException {
        final Object source = getSource( info );
        PageContext ctx = info.getPageContext();
        if ( source instanceof ContentEntityObject && ( ctx == null || !source.equals( info.getContent() ) ) )
            ctx = ( ( ContentEntityObject ) source ).toPageContext();

        // Attempts to set the current content as the root.
        return ReportBuilder.executeRoot( ctx, new Executor<String>() {
            public String execute() throws ReportException {
                String key = getKey( info );
                key = ( key != null && key.trim().length() == 0 ) ? null : key;

                try {
                    Object value = getValue( info, source, key );
                    return AbstractValueMacro.this.report( source, key, value, info );
                } catch ( UnsupportedContextException e ) {
                    throw new ExecutionException( e.getMessage(), e );
                } catch ( SupplierException e ) {
                    throw new ExecutionException( e.getMessage(), e );
                }
            }
        } );
    }

    protected Object getValue( MacroInfo info, final Object source, String key ) throws ReportException {
        try {
            return SupplierAssistant.getInstance().findValue( source, key );
        } catch ( SupplierException e ) {
            throw new ExecutionException( e.getMessage(), e );
        } catch ( UnsupportedContextException e ) {
            throw new ExecutionException( e.getMessage(), e );
        }
    }

    /**
     * Retrieves the key parameter for the current macro.
     * 
     * @param info
     *            the macro info.
     * @return The key for the current macro.
     */
    protected String getKey( MacroInfo info ) {
        IndexedStorage params = info.getMacroParams();
        return params.getString( KEY_PARAM, params.getString( 0, getDefaultKey() ) );
    }

    /**
     * Override this method to change the default key if none is provided.
     * Defaults to <code>null</code>.
     * 
     * @return <code>null</code>
     */
    protected String getDefaultKey() {
        return null;
    }

    protected Object getSource( MacroInfo info ) throws ReportException {
        Object source;
        try {
            source = new ConfluenceEntityParameter( null, "source" ).findEntity( info );
        } catch ( ParameterException e ) {
            throw new ExecutionException( e );
        }

        if ( source == null )
            source = ReportBuilder.getCurrentSource( info.getContent() );

        if ( source == null )
            throw new ExecutionException( "Unable to find a source to report on." );
        return source;
    }

    protected abstract String report( Object item, String key, Object value, MacroInfo info )
            throws ReportException, SupplierException, UnsupportedContextException;
}
