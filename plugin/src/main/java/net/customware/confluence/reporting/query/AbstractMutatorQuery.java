package net.customware.confluence.reporting.query;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import net.customware.confluence.reporting.Sortable;

import org.apache.commons.collections.ComparatorUtils;
import org.randombits.confluence.filtering.criteria.Criteria;
import org.randombits.confluence.support.MacroInfo;

/**
 * An extension of the standard {@link AbstractQuery} which will modify the results
 * of the specified sub-query and return a new set of results.
 * 
 * @param <From> The original type being mutated from.
 * @param <To> The new type being mutated to.
 */
public abstract class AbstractMutatorQuery<From, To, Sort> extends AbstractQuery<To> implements Queryable<From>, Sortable<Sort> {

    private Query<? extends From> query;
    
    private Class<From> fromType;
    
    private Class<To> toType;
    
    private Comparator<? super Sort> comparator;
    
    public AbstractMutatorQuery( Class<From> from, Class<To> to, MacroInfo info ) throws QueryException {
        this( from, to, info, null );
    }

    public AbstractMutatorQuery( Class<From> from, Class<To> to, MacroInfo info, Query<? extends From> query ) throws QueryException {
        super( info );
        this.query = query;
        fromType = from;
        toType = to;
    }

    @Override
    protected void findItems( List<To> items, Criteria criteria ) throws QueryException {
        Results<? extends From> fromItems = null;
        if (query != null )
            fromItems = query.execute();
        
        Iterator<? extends To> toItems = mutate( fromItems );
        
        fromItems.close();
        
        while ( toItems.hasNext() ) {
            items.add( toItems.next() );
        }
    }

    protected abstract Results<? extends To> mutate( Results<? extends From> fromItems ) throws QueryException;

    public Class<From> getFromType() {
        return fromType;
    }

    public Class<To> getToType() {
        return toType;
    }

    public boolean addQuery( Query<? extends From> query ) {
        if ( this.query == null )
            this.query = query;
        return false;
    }

    public boolean canAddQuery() {
        return query == null;
    }

    public Class<From> getQueryValueType() {
        return fromType;
    }

    public Class<To> getValueType() {
        return toType;
    }

    /**
     * Adds a comparator to the query for sorting. This will be chained after
     * any prior comparators.
     */
    public boolean addComparator( Comparator<? super Sort> comparator ) {
        if ( this.comparator == null )
            this.comparator = comparator;
        else
            this.comparator = ComparatorUtils.chainedComparator( this.comparator, comparator );

        return true;
    }

    /**
     * Clear the list of registered comparators.
     */
    protected void clearComparators() {
        this.comparator = null;
    }

    /**
     * Returns the comparator to search based on. If multiple comparators have
     * been added this will be a chain of comparators. Each successive
     * comparator will only be called if the one earlier on the list considers
     * the two objects to be equal.
     */
    public Comparator<? super Sort> getComparator() {
        return comparator;
    }
}
