package net.customware.confluence.reporting.query;

import java.util.ArrayList;
import java.util.List;

import net.customware.confluence.reporting.Filterable;

import org.randombits.confluence.filtering.criteria.Criteria;
import org.randombits.confluence.filtering.criteria.Criterion;
import org.randombits.confluence.filtering.criteria.GroupCriteria;
import org.randombits.confluence.support.MacroInfo;

/**
 * Implements some of the standard features of {@link Query} objects.
 */
public abstract class AbstractQuery<V> implements CachingQuery<V>, Filterable {

    /**
     * The 'matchAll' parameter name.
     */
    public static final String MATCH_ALL_PARAM = "matchAll";

    private GroupCriteria criteria;

    private ArrayList<V> items;

    public AbstractQuery( MacroInfo info ) throws QueryException {
        init( info );
    }

    /**
     * Override this method to initialise the query. Any overriding methods
     * should call <code>super.init( info )</code> before doing their own
     * initialisation.
     * 
     * @throws QueryException
     */
    protected void init( MacroInfo info ) throws QueryException {
        boolean matchAllCriteria = info.getMacroParams().getBoolean( MATCH_ALL_PARAM, true );
        criteria = new GroupCriteria( matchAllCriteria );
    }

    /**
     * Adds the criterion to the list of criteria.
     * 
     * @param criterion
     *            the new criterion.
     */
    public boolean addCriterion( Criterion criterion ) {
        criteria.addCriterion( criterion );
        clearCache();
        return true;
    }

    public void clearCache() {
        items = null;
    }

    /**
     * Returns the set of criteria.
     */
    public Criteria getCriteria() {
        return criteria;
    }

    /**
     * Executes the query, returning an Iterator to allow looping through the
     * results.
     */
    public Results<V> execute() throws QueryException {
        if ( items == null ) {
            items = new java.util.ArrayList<V>();
            findItems( items, criteria );
        }

        return new IteratorResults<V>( items );
    }

    /**
     * This method is called to find the items for this query. Items which match
     * the provided criteria should be added to the <code>items</code> list.
     * 
     * @param items
     *            The list of items.
     * @param criteria
     *            The criteria items should match.
     * @throws QueryException
     *             if there is a problem.
     */
    protected abstract void findItems( List<V> items, Criteria criteria ) throws QueryException;
}
