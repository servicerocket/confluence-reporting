package net.customware.confluence.reporting.query;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.customware.confluence.reporting.Sortable;

import org.apache.commons.collections.ComparatorUtils;
import org.randombits.confluence.filtering.criteria.Criteria;
import org.randombits.confluence.support.MacroInfo;

public abstract class AbstractSortableQuery<V> extends AbstractQuery<V> implements Sortable<V> {

    private Comparator<? super V> comparator;

    public AbstractSortableQuery( MacroInfo info ) throws QueryException {
        super( info );
    }

    @Override
    protected void findItems( List<V> items, Criteria criteria ) throws QueryException {
        findUnsortedItems( items, criteria );

        if ( items.size() > 0 && comparator != null ) {
            Collections.sort( items, comparator );
        }

    }

    protected abstract void findUnsortedItems( List<V> items, Criteria criteria ) throws QueryException;

    /**
     * Adds a comparator to the query for sorting. This will be chained after
     * any prior comparators.
     */
    public boolean addComparator( Comparator<? super V> comparator ) {
        if ( this.comparator == null )
            this.comparator = comparator;
        else
            this.comparator = ComparatorUtils.chainedComparator( this.comparator, comparator );

        clearCache();
        return true;
    }

    /**
     * Clear the list of registered comparators.
     */
    protected void clearComparators() {
        this.comparator = null;
        clearCache();
    }

    /**
     * Returns the comparator to search based on. If multiple comparators have
     * been added this will be a chain of comparators. Each successive
     * comparator will only be called if the one earlier on the list considers
     * the two objects to be equal.
     */
    public Comparator<? super V> getComparator() {
        return comparator;
    }
}
