package net.customware.confluence.reporting.query;

/**
 * Indicates that the query may be caching its results. Allows it to be reset
 * via the {@link #clearCache()} method.
 * 
 * @author David Peterson
 * 
 * @param <V>
 *            The result type.
 */
public interface CachingQuery<V> extends Query<V> {
    /**
     * Clears any cached results that may be being held by the query.
     */
    void clearCache();
}
