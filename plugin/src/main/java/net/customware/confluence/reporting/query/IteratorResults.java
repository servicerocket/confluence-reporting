package net.customware.confluence.reporting.query;

import java.util.Collection;
import java.util.Iterator;

/**
 * A {@link Results} that directly contains an {@link Iterator} and size value.
 * 
 * @param <V>
 *            The returned value type.
 */
public class IteratorResults<V> implements Results<V> {
    private Iterator<V> iterator;

    private int size;

    /**
     * Constructs the result set from the specified collection.
     * 
     * @param collection
     *            The collection.
     */
    public IteratorResults( Collection<V> collection ) {
        this( collection.iterator(), collection.size() );
    }

    /**
     * Constructs the result set from the iterator and size.
     * 
     * @param iterator
     *            The iterator.
     * @param size
     *            The number of values in the iterator.
     */
    public IteratorResults( Iterator<V> iterator, int size ) {
        this.iterator = iterator;
        this.size = size;
    }

    /**
     * The number of items in the result set, or {@link Results#UNKNOWN}
     */
    public int size() {
        return size;
    }

    public void close() throws QueryException {
        // Do nothing.
    }

    public boolean hasNext() {
        return iterator.hasNext();
    }

    public V next() {
        return iterator.next();
    }

    public void remove() {
        iterator.remove();
    }

}
