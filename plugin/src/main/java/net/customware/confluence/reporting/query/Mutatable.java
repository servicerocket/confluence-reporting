package net.customware.confluence.reporting.query;

import org.randombits.facade.Facadable;

/**
 * A context which can have it's results mutated.
 */
@Facadable
public interface Mutatable<F, T> {

    /**
     * The type being mutated from.
     * 
     * @return The from type.
     */
    public Class<F> getFromType();

    /**
     * The type being mutated to.
     * 
     * @return The to type.
     */
    public Class<T> getToType();

    /**
     * Sets the mutator for this context.
     * 
     * @param mutator
     *            the mutator.
     */
    public void setMutator( Mutator<? super F, ? extends T> mutator );

    /**
     * Returns the mutator for this context.
     * 
     * @return the mutator.
     */
    public Mutator<? super F, ? extends T> getMutator();
}
