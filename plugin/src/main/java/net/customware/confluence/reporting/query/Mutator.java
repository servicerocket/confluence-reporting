package net.customware.confluence.reporting.query;

import java.util.Iterator;

import org.randombits.facade.Facadable;

/**
 * This is a specific type of query which can augment another query.
 * It can be registered with {@link Mutatable} instances.
 * 
 * @param <F> The 'from' class.
 * @param <T> The 'to' class.
 */
@Facadable
public interface Mutator<F,T> {
    
    /**
     * Called when the mutator should modify the original list of items.
     * 
     * @param original The original items.
     * @return The modified items.
     * @throws QueryException if there is a problem.
     */
    public Iterator<T> mutate( Iterator<? extends F> original ) throws QueryException;
}
