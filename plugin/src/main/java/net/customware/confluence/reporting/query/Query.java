package net.customware.confluence.reporting.query;

import net.customware.confluence.reporting.Report;

import org.randombits.facade.Facadable;

/**
 * Reporters execute the actual queries which the {@link Report} then iterates
 * through to generate the pretty output.
 * 
 * @param <V>
 *            The value type returned from the query.
 */
@Facadable
public interface Query<V> {

    /**
     * Returns the class type returned by this query.
     * 
     * @return The query class type.
     */
    public Class<V> getValueType();

    /**
     * Executes the query, returning the results.
     * 
     * @return An iterator running through the sorted results.
     * @throws QueryException
     *             if there is a problem.
     */
    Results<V> execute() throws QueryException;
}
