package net.customware.confluence.reporting.query;

import org.randombits.facade.Facadable;

/**
 * Contexts which will accept queries should implement this class.
 * {@link net.customware.confluence.reporting.ReportSetup} is one such context.
 */
@Facadable
public interface Queryable<T> {
    
    /**
     * The type which must be returned by the query.
     *  
     * @return The query return type.
     */
    public Class<T> getQueryValueType();
    
    /**
     * Checks if any further queries can be added.
     */
    public boolean canAddQuery();

    /**
     * Adds the specified query.
     * 
     * @param query
     *            The new query.
     * @return <code>false</code> if the query could not be added.
     */
    public boolean addQuery( Query<? extends T> query );
}
