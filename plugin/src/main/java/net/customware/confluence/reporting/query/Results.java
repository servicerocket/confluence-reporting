package net.customware.confluence.reporting.query;

import java.util.Iterator;

import org.randombits.facade.Facadable;

/**
 * Provides access to the result set, as well as the number of items. Instances
 * of this type are {@link Iterable}, so they can be used with the
 * <code>for ( V x : y )</code> syntax.
 * 
 * @param <V>
 *            The value type.
 */
@Facadable
public interface Results<V> extends Iterator<V> {
    /**
     * The size for result sets whose size is unknown (-1).
     */
    static int UNKNOWN = -1;

    /**
     * Returns the number of items in the result set, or {@link #UNKNOWN}.
     * 
     * @return The result set size or {@link #UNKNOWN}.
     */
    int size();

    /**
     * This should be called after the results have been processed, so that any
     * cleanup code can be run.
     */
    void close() throws QueryException;
}
