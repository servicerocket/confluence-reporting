/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.reporting.supplier;

import org.randombits.confluence.supplier.Supplier;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;

import java.util.List;
import java.util.Iterator;

/**
 * Abstract base class for suppliers which work with Confluence content.
 * 
 * @author David Peterson
 */
public abstract class AbstractConfluenceSupplier implements Supplier {
    private UserAccessor userAccessor;

    private BootstrapManager bootstrapManager;

    private PermissionManager permissionManager;

    private LabelManager labelManager;

    private SpaceManager spaceManager;

    protected UserAccessor getUserAccessor() {
        if ( userAccessor == null )
            userAccessor = ( UserAccessor ) ContainerManager.getComponent( "userAccessor" );

        return userAccessor;
    }

    protected BootstrapManager getBootstrapManager() {
        if ( bootstrapManager == null )
            bootstrapManager = ( BootstrapManager ) ContainerManager.getComponent( "bootstrapManager" );

        return bootstrapManager;
    }

    protected String getContextPath() {
        return getBootstrapManager().getWebAppContextPath();
    }

    protected <T> List<T> getPermittedObjects( List<T> contentList ) {
        if ( contentList != null ) {
            Iterator<T> i = contentList.iterator();
            T content;

            while ( i.hasNext() ) {
                content = i.next();
                if ( !getPermissionManager().hasPermission( getCurrentUser(), Permission.VIEW, content ) )
                    i.remove();
            }
        }
        return contentList;
    }

    protected static User getCurrentUser() {
        return AuthenticatedUserThreadLocal.getUser();
    }

    protected PermissionManager getPermissionManager() {
        if ( permissionManager == null )
            permissionManager = ( PermissionManager ) ContainerManager.getComponent( "permissionManager" );
        return permissionManager;
    }

    public LabelManager getLabelManager() {
        if ( labelManager == null )
            labelManager = ( LabelManager ) ContainerManager.getComponent( "labelManager" );

        return labelManager;
    }

    protected List<Label> getPermittedLabels( List<Label> labels ) {
        if ( labels != null ) {
            Iterator<Label> i = labels.iterator();
            Label rel;
            User user = getCurrentUser();
            String username = user != null ? user.getName() : null;

            while ( i.hasNext() ) {
                rel = i.next();
                if ( !rel.isVisibleTo( username ) )
                    i.remove();
            }
        }

        return labels;
    }

    protected SpaceManager getSpaceManager() {
        if ( spaceManager == null )
            spaceManager = ( SpaceManager ) ContainerManager.getComponent( "spaceManager" );
        return spaceManager;
    }
}
