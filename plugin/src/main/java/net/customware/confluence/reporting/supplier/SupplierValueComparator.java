/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.reporting.supplier;

import org.apache.log4j.Logger;
import org.randombits.confluence.supplier.SupplierAssistant;
import org.randombits.confluence.supplier.SupplierException;
import org.randombits.confluence.supplier.UnsupportedContextException;

import java.util.Comparator;

/**
 * Wraps a standard comparator with one which will look for the provided key
 * value with the {@link org.randombits.confluence.supplier.SupplierAssistant}.
 * 
 * @author David Peterson
 * 
 * @param <T> The source type
 * @param <V> The type of the value at the specified key.
 */
public class SupplierValueComparator<T,V> implements Comparator<T> {
    private static final Logger LOG = Logger.getLogger( SupplierValueComparator.class );

    private String key;

    private Comparator<? super V> comparator;

    private boolean renderWiki;

    public SupplierValueComparator( String key, Comparator<? super V> comparator ) {
        this( key, comparator, false );
    }

    public SupplierValueComparator( String key, Comparator<? super V> comparator, boolean renderWiki ) {
        this.key = key;
        this.comparator = comparator;
        this.renderWiki = renderWiki;
    }

    /**
     * Compares its two arguments for order. Returns a negative integer, zero,
     * or a positive integer as the first argument is less than, equal to, or
     * greater than the second.
     * <p>
     * <p/> The implementor must ensure that <tt>sgn(compare(x, y)) ==
     * -sgn(compare(y, x))</tt>
     * for all <tt>x</tt> and <tt>y</tt>. (This implies that
     * <tt>compare(x, y)</tt> must throw an exception if and only if
     * <tt>compare(y, x)</tt> throws an exception.)
     * <p>
     * <p/> The implementor must also ensure that the relation is transitive:
     * <tt>((compare(x, y)&gt;0) &amp;&amp; (compare(y, z)&gt;0))</tt> implies
     * <tt>compare(x, z)&gt;0</tt>.
     * <p>
     * <p/> Finally, the implementer must ensure that <tt>compare(x, y)==0</tt>
     * implies that <tt>sgn(compare(x, z))==sgn(compare(y, z))</tt> for all
     * <tt>z</tt>.
     * <p>
     * <p/> It is generally the case, but <i>not</i> strictly required that
     * <tt>(compare(x, y)==0) == (x.equals(y))</tt>. Generally speaking, any
     * comparator that violates this condition should clearly indicate this
     * fact. The recommended language is "Note: this comparator imposes
     * orderings that are inconsistent with equals."
     * 
     * @param o1
     *            the first object to be compared.
     * @param o2
     *            the second object to be compared.
     * @return a negative integer, zero, or a positive integer as the first
     *         argument is less than, equal to, or greater than the second.
     * @throws ClassCastException
     *             if the arguments' types prevent them from being compared by
     *             this Comparator.
     */
    public int compare( T t1, T t2 ) {
        V v1;
        V v2;
        if ( key != null ) {
            try {
                v1 = ( V ) SupplierAssistant.getInstance().findValue( t1, key, renderWiki );
            } catch ( UnsupportedContextException e ) {
                LOG.warn( "Unsupported context on key: " + key, e );
                return -1;
            } catch ( SupplierException e ) {
                LOG.warn( "Supplier exception on key: " + key, e );
                return -2;
            }

            try {
                v2 = ( V ) SupplierAssistant.getInstance().findValue( t2, key, renderWiki );
            } catch ( UnsupportedContextException e ) {
                LOG.warn( "Unsupported context on key: " + key, e );
                return 1;
            } catch ( SupplierException e ) {
                LOG.warn( "Supplier exception on key: " + key, e );
                return 2;
            }

        } else {
            v1 = ( V ) t1;
            v2 = ( V ) t2;
        }

        try {
            return comparator.compare( v1, v2 );
        } catch ( ClassCastException e ) {
            throw new SupplierValueException( key, "Unsupported class type", e );
        } catch ( RuntimeException e ) {
            throw new SupplierValueException( key, null, e );
        }
    }

    public boolean isRenderWiki() {
        return renderWiki;
    }

    public void setRenderWiki( boolean renderWiki ) {
        this.renderWiki = renderWiki;
    }
}
