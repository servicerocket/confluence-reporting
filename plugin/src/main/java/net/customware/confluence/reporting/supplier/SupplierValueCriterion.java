/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.reporting.supplier;

import java.util.Collection;

import org.randombits.confluence.filtering.criteria.Criterion;
import org.randombits.confluence.filtering.criteria.SourceCriterion;
import org.randombits.confluence.supplier.SupplierAssistant;
import org.randombits.confluence.supplier.UnsupportedContextException;
import org.randombits.confluence.supplier.SupplierException;
import org.randombits.facade.Facadable;
import org.apache.log4j.Logger;

/**
 * This criterion is designed to wrap other standard criterion, substituting the
 * supplied object to match with one found with the SupplierAssistant and the
 * provided <code>key</code>.
 * 
 * @author David Peterson
 */
@Facadable
public class SupplierValueCriterion implements SourceCriterion<Object> {
    private static final Logger LOG = Logger.getLogger( SupplierValueCriterion.class );

    private String key;

    private Criterion criterion;

    public SupplierValueCriterion( String key, Criterion criterion ) {
        this.key = key;
        this.criterion = criterion;
    }

    /**
     * Checks if the object matches this criterion.
     * 
     * @param object
     *            The object.
     * @return <code>true</code> if the object matches this criterion.
     */
    public boolean matches( Object object ) {
        if ( key != null ) {
            try {
                object = SupplierAssistant.getInstance().findValue( object, key );
            } catch ( UnsupportedContextException e ) {
                LOG.warn( "Unsupported context for key: " + key, e );
                return false;
            } catch ( SupplierException e ) {
                LOG.error( "Supplier exception for key: " + key, e );
            }
        }

        return criterion.matches( object );
    }

    public String getKey() {
        return key;
    }

    public void setKey( String key ) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "{supplier; key: " + key + "; criterion: " + criterion + "}";
    }

    public Collection<Object> getMatchingValues() {
        if ( criterion instanceof SourceCriterion<?> ) {
            return ( ( SourceCriterion<Object> ) criterion ).getMatchingValues();
        } else {
            return null;
        }
    }

    public SourceCriterion.Weight getWeight() {
        return Weight.MEDIUM;
    }
}
