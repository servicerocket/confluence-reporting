package net.customware.confluence.reporting.supplier;

public class SupplierValueException extends RuntimeException {

    private String key;

    public SupplierValueException() {
        this( null, null, null );
    }

    public SupplierValueException( String key, String message, Throwable e ) {
        super( key != null ? "Error while comparing '" + key + "': " + ( message != null ? message + " - " : "" )
                + e.getLocalizedMessage() : null, e );
        this.key = key;
    }

    public SupplierValueException( String key ) {
        this( key, null, null );
    }

    public SupplierValueException( Throwable e ) {
        this( null, null, e );
    }

    public String getKey() {
        return key;
    }

}
